taskjuggler.so: src/parser.c
	$(CC) -shared -fPIC -g -O2 -I src src/parser.c -o taskjuggler.so

src/parser.c: grammar.js
	npx tree-sitter generate
