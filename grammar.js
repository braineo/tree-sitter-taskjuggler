module.exports = grammar({
    name: 'taskjuggler',

    extras: $ => [/[ \t]/, $.macro_call, $.variable_reference, $.comment],

    rules: {
        source_file: $ => repeat($._thing),

        _thing: $ => choice('\n', seq($.attribute, '\n')),

        attribute: $ => prec.left(1, seq(
            prec.left(2, field('name', choice($.new_thing_name, $.attribute_name))),
            optional(choice(
                $.arguments,
                seq('(', $.arguments, ')')
            )),
            optional(seq(
                '{',
                repeat($._thing),
                '}',
            )),
        )),

        new_thing_name: $ => choice('task', 'resource', 'project'),

        attribute_name: $ =>
            seq(
                optional(
                    seq(field('scenario', $.identifier), token.immediate(':')),
                ),
                field('name', $.identifier),
            ),

        arguments: $ => choice($.whitespace_arguments, $.comma_arguments),

        whitespace_arguments: $ => prec.left(2, repeat1($.argument)),

        comma_arguments: $ => prec.left(1, repeat1Comma($.argument)),

        argument: $ =>
            choice(
                $.identifier_reference,
                $.string,
                $.duration,
                $.logical_expression,
                $.variable_reference,
                $.interval2,
                $.date,
                $.workinghours,
                $.number,
            ),

        workinghours: $ => seq(
            repeat1Comma(seq(
                $.weekday,
                optional(seq('-', $.weekday))
            )),
            choice(
                'off',
                prec.left(2, repeat1Comma(seq($.time, '-', $.time))),
            ),
        ),

        interval2: $ =>
            seq($.date, choice(seq('-', $.date), seq('+', $.duration))),

        macro_call: $ =>
            seq('${', field('name', $.identifier), repeat($.string), '}'),

        variable_reference: $ => seq('${', /\d+/, '}'),

        weekday: $ => choice('sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'),

        report_format: $ => choice('csv', 'html', 'niku'),

        date: $ => /\d{4}-\d{2}-\d{2}(?:-\d{1,2}:\d{2}(?::\d{2})?)?/,

        time: $ => /\d{1,2}:\d{2}(?::\d{2})?/,

        duration: $ => seq(/\d+(\.\d+)?/, choice('min', 'h', 'd', 'w', 'm', 'y')),

        logical_expression: $ => choice('@none', '@all'),

        identifier: $ => /([a-zA-Z]|\$\{\d+\})(\w|\$\{\d+\})*/,

        identifier_reference: $ => choice(
            $.absolute_identifier,
            $.identifier,
            $.relative_identifier,
        ),

        absolute_identifier: $ => seq(
            $.identifier,
            repeat1(seq('.', $.identifier))
        ),

        relative_identifier: $ => seq(repeat1('!'), $.identifier),

        number: $ => /\d+(\.\d+)?/,

        string: $ =>
            choice(
                $.single_quote_string,
                $.double_quote_string,
                $.multiline_string,
            ),

        single_quote_string: $ => seq("'", repeat(choice(/[^']/, "\\'")), "'"),

        double_quote_string: $ => seq('"', repeat(choice(/[^"]/, '\\"')), '"'),

        multiline_string: $ =>
            seq(
                $.multiline_string_start,
                repeat(choice(/[^-]/, /-[^>]/, /->[^8]/)),
                $.multiline_string_end,
            ),

        multiline_string_start: $ => '-8<-',

        multiline_string_end: $ => '->8-',

        comment: $ => token(choice(
            seq('#', /.*/, '\n'),
            seq(
                '/*',
                /[^*]*\*+([^/*][^*]*\*+)*/,
                '/',
            ),
      )),

    },
});

function repeat1Comma(rule) {
    return seq(rule, repeat(seq(',', rule)));
}
